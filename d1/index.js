// Repitition Control Structure

// Do...while
// let m = Number(prompt("Give me number:"));
// console.log(m);

let n = 0;
console.log(`Starting value of n is ${n}`);
do {
  console.log(`Item number ${n} from 'do while loop'.`);
  n++;
} while (n < 10);

while (n < 20) {
  console.log(`Item number ${n} from 'while loop'.`);
  n++;
}

console.log(`Curret value of n is ${n} after loops`);
// For...loop

let nm = 0;
while (nm < 5) {
  nm++;
  console.log(`${nm}`);
}

function printThisXTimes(msg, n) {
  let m = 0;
  do {
    m++;
    console.log(`${msg} !, ${m}x`);
  } while (m < n);
}

// printThisXTimes("Hello", 10);

let m = 0;
do {
  m++;
  console.log(`${m}x`);
} while (m < n);

// Foor Loop
/*
  for(initialization; condition; finalExpression){
    code block
  }
*/

for (let i = 0; i <= 10; ++i) {
  console.log(`${i}x`);
}

let fruits = ["Apple", "Durian", "Kiwi", "Pineapple", "Strawberry"];
console.log(fruits[0]);
console.log(fruits[4]);
console.log(fruits[fruits.length - 1]);

let arr = [
  "Singapore",
  "China",
  "Philippines",
  "India",
  "Indonesia",
  "Malaysia",
];
for (let i = 0; i < arr.length - 1; ++i) {
  console.log(arr[i]);
}

arr[7] = "Japan";
console.log(arr);

// Array of Objects
let cars = [
  { brand: "Toyota", type: "Sedan" },
  { brand: "Rolls Royce", type: "Luxury Sedan" },
  { brand: "Mazda", type: "Hatchback" },
];
console.log(cars);

let myName = "NelSon deLa TorRe";
for (let i = 0; i < myName.length; i++) {
  let a = myName[i].toLocaleLowerCase();
  if (a == "a" || a == "e" || a == "i" || a == "o" || a == "u") {
    console.log("3");
  } else {
    console.log(a);
  }
}

console.clear();
for (let a = 20; a > 0; a--) {
  // if (a % 2 === 0) {
  //   continue;
  // }
  if (a < 10) {
    break;
  }
  console.log(a);
}
